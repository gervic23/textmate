This website is designed and developed by myself for all text addicts, for texters who are looking for a textmate. This website was written first in Codeigniter 2.2.0 and updated to 3.0 with mobile compatibility. This is my first website developing in Codeigniter framework.

This source code is not intended to publish online. Please do not upload this source code to a webhost, or share to anyone. I only use this as a part of my portfolio.

======== INSTALLATION ========

Clone this repository to your web directory (htdocs, public_html).

== FOR WINDOWS XAMPP ==

* Copy and pase this tag to C:\xampp\apache\conf\extra\httpd-vhosts.conf

<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot "C:/xampp/htdocs/textmate"
    ServerName textmate.dev
	ServerAlias www.textmate.dev
</VirtualHost>

* Add this code to C:\WINDOWS\SYSTEM32\Drivers\etc\hosts

127.0.01	textmate.dev

* Create a database "textmate".
* Import textmate.sql to the database.
* Open application/config/database.php and change your database credentials.
* Restart apache.

== For Linux LAMP ==

* Run this command to your command terminal...

sudo gedit /etc/apache2/sites-available/000-default.conf

* Copy and paste this tag...

<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot "/var/www/textmate"
    ServerName textmate.dev
    ServerAlias www.textmate.dev
</VirtualHost>

* Run this command to your command terminal...

sudo gedit /etc/hosts

* Add this code to your hosts file.

127.0.01	textmate.dev

* Create a database "textmate".
* Import textmate.sql to the database.
* Open application/config/database.php and change your database credentials.
* Restart apache by this command.

sudo systemctl restart apache2


== SYSTEM REQUIREMENTS ==
* PHP 5.6 or higher

== Admin Priviledge ==

http://textmate.dev/admincp

Username: admin
Password: onegervic23


