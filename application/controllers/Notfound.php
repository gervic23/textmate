<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends CI_Controller {
  private $canonical;

  public function __construct() {

    //Load Parent CI Controller Constructor.
    parent::__construct();

    //Default Canonical.
    $this->canonical = base_url();

    //Load Form Helper
    $this->load->helper( 'form' );

    //Load HTML Helper.
    $this->load->helper( 'html' );
  }

  public function index() {
    //Header Title.
    $default['title'] = "Looking For Textmate? - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical;

    //Page view name.
    $default['page'] = 'notfound';

    //Load View For This Page.
    $this->load->view('textmate/body/body', $default);
  }
}
