<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
  public function __construct() {
    parent::__construct();

    //Load model.
    $this->load->model( 'results' );

    //Load Cookie Helper.
    $this->load->helper( 'cookie' );

    //Load Session Library.
    $this->load->library( 'session' );

    //Load File Helper.
    $this->load->helper( 'file' );
  }

  public function index() {
    //Redirect users to homepage if user attempt to access this url.
    redirect('/');
  }

  /************************************************
  * Method to detect if the request is AJAX or not.
  *************************************************/
  private function is_ajax() {
    if ( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ) {
      return true;
    }

    return false;
  }

  /***************************************************************
  * Load More Results. Once the Read More Button on the cat page.
  ****************************************************************/
  public function more_results() {
    //Check if has POST request and AJAX request.
    if ( $_POST && $this->is_ajax() ) {
      //Network Post Value Reqeust sends.
      $network = $this->input->post( 'network' );

      //Rows Post Value Request sends.
      $rows = $this->input->post( 'rows' );

      //Get Category Messages.
      $messages = $this->results->more_posts( $network, $rows );
      ?>

      <?php
      //Update Category Results.
      ?>
      <ul>
        <?php foreach ( $messages as $message ): ?>
          <li class="post">
            <div class="message"><?php echo $message->message; ?></div>
            <ul>
              <li>Name: <?php echo $message->name; ?></li>
              <li>Gender: <?php echo $message->gender; ?></li>
              <li>Category: <?php echo $message->network; ?></li>
              <li>Date: <?php echo $message->date; ?></li>
            </ul>
          </li>
        <?php endforeach; ?>
      </ul>

      <script type="text/javascript">
        $(".post-messages ul li.post").addClass("hidden").viewportChecker({
          classToAdd: "visible animated bounceInLeft",
          offset: 100
        });
      </script>
      <?php
    }
  }

  /*****************************
  * Method for posting message.
  ******************************/
  public function post_message() {
    //Check if has POST request and AJAX request.
    if ( $_POST && $this->is_ajax() ) {
      //Name Post Value Request.
      $name = $this->security->xss_clean( $this->input->post( 'name' ) );

      //Gender Post Value Request.
      $gender = $this->security->xss_clean( $this->input->post( 'gender' ) );

      //Message Post Value Request.
      $message = $this->security->xss_clean( nl2br( $this->input->post( 'message' ) ) );

      //Network Post Value Request.
      $network = $this->security->xss_clean( $this->input->post( 'network' ) );

      //Date Format.
      $date = date( "Y-m-d H:i:s" );

      /**************************
      * Filter offensive words.
      ***************************/
      //Offensive words on array set.
      $old = array();
      $old[] = 'fuck';
      $old[] = 'sex';
      $old[] = 'shit';
      $old[] = 'puta';
      $old[] = 'seb';
      $old[] = 'tae';
      $old[] = 'tang';
      $old[] = 'malibog';
      $old[] = 'whore';

      //Replace offensive words on array set.
      $new = array();
      $new[] = '****';
      $new[] = '***';
      $new[] = '****';
      $new[] = '****';
      $new[] = '***';
      $new[] = '***';
      $new[] = '****';
      $new[] = '*******';
      $new[] = '*****';

      //Apply filters.
      $message = str_ireplace( $old, $new, $message );

      if ( get_cookie( 'block', true ) ) {
        ?>

        <div class="block-container">
          You already madbe your post. Please try again later.
        </div>

        <?php
      } else {
        //Set cookie to blocl user to post message again to avoid flooding.
        set_cookie( 'block', 1, 120, '', '/' );

        //Set Post Values to an Array.
        $data = array(
          'id' => '',
          'name' => $name,
          'gender' => $gender,
          'network' => $network,
          'message' => $message,
          'ip' => $this->input->ip_address(),
          'date' => $date
        );

        //Insert Post Values to the database.
        $this->results->submit_post( $data );

        ?>

        <?php
          //Show new Message to the Category page.
        ?>
        <div class="ajax-single-post">
          <ul>
            <li class="post">
              <div class="message"><?php echo $message; ?></div>
              <ul>
                <li>Name: <?php echo $name; ?></li>
                <li>Gender: <?php echo $gender; ?></li>
                <li>Category: <?php echo $network; ?></li>
                <li>Date: <?php echo $date ; ?></li>
              </ul>
            </li>
          </ul>
        </div>

        <?php
      }
    }
  }

  /*****************************************
  *  Load More Results for search keywords.
  ******************************************/
  public function search_more_results() {
    //Check if has POST request and AJAX request.
    if ( $_POST && $this->is_ajax() ) {
      //Get Request Keyword.
      $keywords = $this->input->post( 'keywords' );

      //Get Request rows.
      $rows = $this->input->post( 'rows' );

      //Explode Keywords via spaces.
      $keywords = explode( ' ', $keywords );

      //Get the results.
      $messages = $this->results->search_more_post( $keywords, $rows );
      ?>
      <ul>
        <?php foreach ( $messages as $message ): ?>
          <li class="post">
            <div class="message"><?php echo $message->message; ?></div>
            <ul>
              <li>Name: <?php echo $message->name; ?></li>
              <li>Gender: <?php echo $message->gender; ?></li>
              <li>Category: <?php echo $message->network; ?></li>
              <li>Date: <?php echo $message->date; ?></li>
            </ul>
          </li>
        <?php endforeach; ?>
      </ul>

      <script type="text/javascript">
        $(".post-messages ul li.post").addClass("hidden").viewportChecker({
          classToAdd: "visible animated bounceInLeft",
          offset: 100
        });
      </script>

      <?php
    }
  }

  /********************************************
  * Get data from tracker.js as web statistic.
  ********************************************/
  public function tracker() {
    //Check if has POST request and AJAX request.
    if ( $_POST &&  $this->is_ajax() ) {
      //Visitor's IP Address.
      $ip = $this->input->post( 'ip' );

      //Visitor's Hostname.
      $hostname = $this->input->post( 'hostname' );

      //Visitor's City.
      $city = $this->input->post( 'city' );

      //Visitor's Region.
      $region = $this->input->post( 'region' );

      //Visitor's Country.
      $country = $this->input->post( 'country' );

      //Visitor's Location Map Coordinates.
      $loc = $this->input->post( 'loc' );

      //Visitor's ISP.
      $isp = $this->input->post( 'org' );

      //Date Format.
      $date = date( "Y-m-d H:i:s" );


      //Setup Array Value.
      $data = array(
        'id' => '',
        'ip' => $ip,
        'hostname' => $hostname,
        'city' => $city,
        'region' => $region,
        'country' => $country,
        'loc' => $loc,
        'isp' => $isp,
        'date' => $date
      );

      //Insert data to database.
      $this->results->insert_stats( $data );
    }
  }

  /**********************************
  * Load Messages for admincp/posts.
  ***********************************/
  public function ajax_posts() {
      //Check if has AJAX request and User is Logged in.
    if ( $this->is_ajax() && $this->session->userdata( 'logged_in' ) ) {
      //Messages from database with 50 results.
      $messages = $this->results->get_messages();
      ?>

      <div class="post-ajax-contents">
        <ul>
          <?php foreach ( $messages as $message ): ?>
            <li class="list">
              <div class="post-message">
                <?php echo $message->message;  ?>
              </div>
              <ul>
                <li>Name: <?php echo $message->name; ?></li>
                <li>Gender <?php echo $message->gender; ?></li>
                <li>Category: <?php echo $message->network; ?></li>
                <li>IP: <?php echo $message->ip; ?></li>
                <li>Date: <?php echo $message->date; ?></li>
              </ul>
              <img onclick="javascript: delete_message( <?=$message->id; ?> )" class="close" src="/pub/img/close.png" />
            </li>
          <?php endforeach; ?>
        </ul>
      </div>

      <div class="post-ajax-btn-container">
        <span class="post-ajax-btn">Show More</span>
      </div>

      <div class="post-ajax-loading-container">
        <img src="/pub/img/loading3.gif" />
      </div>

      <script type="text/javascript">
        var rows = 50;

        $(".post-ajax-btn").on("click", function() {
          $(".post-ajax-btn-container").hide();
          $(".post-ajax-loading-container").show();

          rows = rows + 50;

          var data = {
            "rows": rows
          };

          $.post("/ajax/ajax_post_more/", data, function( r ) {
            $(".post-ajax-loading-container").hide();
            $(".post-ajax-btn-container").show();
            $(".post-ajax-contents").html( r );
          });
        });

        function delete_message( id ) {
          var data = { "id": id };

          $.post("/ajax/delete_message/", data, function( r ) {
            if ( r == 1 ) {
              var data2 = { "rows": rows };
              $.post("/ajax/ajax_post_more/", data2, function( r2 ) {
                $(".post-ajax-contents").html( r2 );
              });
            }
          });
        }
      </script>

      <?php
    }
  }

  /**************************************
  * Load more messages in admincp/posts.
  ***************************************/
  public function ajax_post_more() {
    //Check if has post request, is ajax request and has session.
    if ( $_POST && $this->is_ajax() && $this->session->userdata( 'logged_in' ) ) {
      //Number of results to be shown and value of row post request.
      $rows = $this->input->post( 'rows' );

      //Get Messages Results.
      $messages = $this->results->load_more_messages( $rows );

      ?>

        <ul>
          <?php foreach ( $messages as $message ): ?>
            <li class="list">
              <div class="post-message">
                <?php echo $message->message;  ?>
              </div>
              <ul>
                <li>Name: <?php echo $message->name; ?></li>
                <li>Gender <?php echo $message->gender; ?></li>
                <li>Category: <?php echo $message->network; ?></li>
                <li>IP: <?php echo $message->ip; ?></li>
                <li>Date: <?php echo $message->date; ?></li>
              </ul>
              <img onclick="javascript: delete_message( <?=$message->id; ?> )" class="close" src="/pub/img/close.png" />
            </li>
          <?php endforeach; ?>
        </ul>

      <?php
    }
  }

  /**********************
  * Delete Post Message.
  ***********************/
  public function delete_message() {
    //If has POST Request, has AJAX Request and has SESSION.
    if ( $_POST && $this->is_ajax() && $this->session->userdata( 'logged_in' ) ) {
      $id = $this->input->post( 'id' );
      $this->results->del_msg( $id );
      echo 1;
    }
  }

  /*************
  * Clear Logs
  **************/
  public function clear_logs() {
    //If has POST Request, has AJAX Request and has SESSION.
    if ( $_POST && $this->is_ajax() && $this->session->userdata( 'logged_in' ) ) {
      //Log type from post request.
      $log = $this->input->post( 'log' );

      //Log Types.
      switch ( $log ) {
        case 'search':
          //Check if keywords.json exists to avoid errors.
          if ( file_exists( './pub/logs/keywords.json' ) ) {
            //Get the file contents.
            $keywords = file_get_contents( './pub/logs/keywords.json' );

            //Remove the contents of the file.
            write_file( './pub/logs/keywords.json', '', 'w' );
          }
        break;

        case 'statistics':
          //Delete all entries from statistic table in the database.
          $this->results->clear_statistics();
        break;
      }
    }
  }

  /**************************
  * Load Statistics Details.
  ***************************/
  public function load_statistics() {
    //If has POST Request, has AJAX Request and has SESSION.
    if ( $this->is_ajax() && $this->session->userdata( 'logged_in' ) ) {
      //Get Statistics Details from database.
      $stats = $this->results->get_statistics_details();
      ?>

      <div class="stats-details-logs">
        <?php if ( $stats ): ?>
          <ul>
            <?php foreach ( $stats as $stat ): ?>
              <li class="list">
                <ul>
                  <li>IP: <?=$stat->ip; ?></li>
                  <li>Hostname: <?=$stat->hostname; ?></li>
                  <li>City: <?=$stat->city; ?></li>
                  <li>Country: <?=$stat->country; ?></li>
                  <li>ISP: <?=$stat->isp; ?></li>
                  <li>Date: <?=$stat->date; ?></li>
                </ul>
              </li>
            <?php endforeach; ?>
          </ul>
        <?php else: ?>
          <div class="nostats">There are no statistics at this time.</div>
        <?php endif; ?>
      </div>

      <?php
    }
  }

  /***************
  * Delete Admin
  ****************/
  public function del_admin() {
    //If has POST Request, has AJAX Request and has SESSION.
    if ( $this->is_ajax() && $this->session->userdata( 'logged_in' ) ) {
      echo 'This Action is not available at this time.';
    }
  }
}
