<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
  private $canonical;

  public function __construct() {

    //Load Parent CI Controller Constructor.
    parent::__construct();

    //Default Canonical.
    $this->canonical = base_url();

    //Load HTML Helper.
    $this->load->helper( 'html' );

    //Include model.
    $this->load->model('results');

    //Load Form Helper
    $this->load->helper( 'form' );
  }

  /*
  * Homepage Controller.
  */
  public function index() {

    //Max String length of Categories Last Post.
    $max_len = 35;

    //Header Title.
    $default['title'] = "Looking For Textmate? - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical;

    //Page view name.
    $default['page'] = 'home';

    //Globe Textmate Number of Posts.
    $default['globe_num'] = $this->results->post_num( 'Globe Textmate' );

    //Get User's name for Globe.
    $default['globe_user_name'] = $this->results->user_name( 'Globe Textmate' );

    //Get Globe Last Post.
    $globe_last_post = $this->results->last_post( 'Globe Textmate' );

    $default['globe_post'] = ( strlen( $globe_last_post ) >= $max_len ? substr( $globe_last_post, 0, $max_len ).'...' : $globe_last_post );

    //Smart Textmate Number of Posts.
    $default['smart_num'] = $this->results->post_num( 'Smart Textmate' );

    //Get User's name for Smart.
    $default['smart_user_name'] = $this->results->user_name( 'Smart Textmate' );

    //Get Smart Last Post.
    $smart_last_post = $this->results->last_post( 'Smart Textmate' );

    $default['smart_post'] = ( strlen( $smart_last_post ) >= $max_len ? substr( $smart_last_post, 0, $max_len ).'...' : $smart_last_post );

    //Sun Textmate Number of Posts.
    $default['sun_num'] = $this->results->post_num( 'Sun Textmate' );

    //Get User's name for Sun.
    $default['sun_user_name'] = $this->results->user_name( 'Sun Textmate' );

    //Get Sun Last Post.
    $sun_last_post = $this->results->last_post( 'Sun Textmate' );

    $default['sun_post'] = ( strlen( $sun_last_post ) >= $max_len ? substr( $sun_last_post, 0, $max_len ).'...' : $sun_last_post );

    //TM Textmate Number of Posts.
    $default['tm_num'] = $this->results->post_num( 'TM Textmate' );

    //Get User's name for TM.
    $default['tm_user_name'] = $this->results->user_name( 'TM Textmate' );

    //Get TM Last Post.
    $tm_last_post = $this->results->last_post( 'TM Textmate' );

    $default['tm_post'] = ( strlen( $tm_last_post ) >= $max_len ? substr( $tm_last_post, 0, $max_len ).'...' : $tm_last_post );

    //TNT Textmate Number of Posts.
    $default['tnt_num'] = $this->results->post_num( 'TNT Textmate' );

    //Get User's name for TNT.
    $default['tnt_user_name'] = $this->results->user_name( 'TNT Textmate' );

    //Get TNT Last Post.
    $tnt_last_post = $this->results->last_post( 'TNT Textmate' );

    $default['tnt_post'] = ( strlen( $tnt_last_post ) >= $max_len ? substr( $tnt_last_post, 0, $max_len ).'...' : $tnt_last_post );

    //Red Textmate Number of Posts.
    $default['red_num'] = $this->results->post_num( 'Red Textmate' );

    //Get User's name for Red.
    $default['red_user_name'] = $this->results->user_name( 'Red Textmate' );

    //Get Red Last Post.
    $red_last_post = $this->results->last_post( 'Red Textmate' );

    $default['red_post'] = ( strlen( $red_last_post ) >= $max_len ? substr( $red_last_post, 0, $max_len ).'...' : $red_last_post );

    //Text Clan Number of Posts.
    $default['clan_num'] = $this->results->post_num( 'Text Clan' );

    //Get User's name for Text Clan.
    $default['clan_user_name'] = $this->results->user_name( 'Text Clan' );

    //Get Text Clan Last Post.
    $clan_last_post = $this->results->last_post( 'Text Clan' );

    $default['clan_post'] = ( strlen( $clan_last_post ) >= $max_len ? substr( $clan_last_post, 0, $max_len ).'...' : $clan_last_post );

    //Quotes Number of Posts.
    $default['quotes_num'] = $this->results->post_num( 'Text Quotes' );

    //Get User's name for Quotes.
    $default['quotes_user_name'] = $this->results->user_name( 'Text Quotes' );

    //Get Quotes Last Post.
    $quotes_last_post = $this->results->last_post( 'Text Quotes' );

    $default['quotes_post'] = ( strlen( $quotes_last_post ) >= $max_len ? substr( $quotes_last_post, 0, $max_len ).'...' : $quotes_last_post );

    //Relationship Number of Posts.
    $default['rel_num'] = $this->results->post_num( 'Relationship' );

    //Get User's name for Relationship.
    $default['rel_user_name'] = $this->results->user_name( 'Relationship' );

    //Get Relationship Last Post.
    $rel_last_post = $this->results->last_post( 'Relationship' );

    $default['rel_post'] = ( strlen( $rel_last_post ) >= $max_len ? substr( $rel_last_post, 0, $max_len ).'...' : $rel_last_post );

    //Load View For This Page.
    $this->load->view('textmate/body/body', $default);
  }
}
