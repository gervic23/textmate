<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admincp extends CI_Controller {
  private $canonical;

  //Encryption for passwords. (Important! Do not change this or otherwise you have to update each users passwords.).
  private $salt = 'da39a3ee5e6b4b0d3255bfef95601890afd80709';

  public function __construct() {
    //Load Main Constructor.
    parent::__construct();

    //Load Model.
    $this->load->model( 'results' );

    //Default Canonical.
    $this->canonical = base_url();

    //Load HTML Helper.
    $this->load->helper( 'html' );

    //Load Form Helper
    $this->load->helper( 'form' );

    //Load Form Validation Library.
    $this->load->library( 'form_validation' );

    //Load Session Library.
    $this->load->library( 'session' );

    //Load Cookie Helper.
    $this->load->helper( 'cookie' );

    /************************************
      Remove expired captcha images.
    ************************************/
    //Expiration time.
    $exp = time() - 10;

    //Get Captchas data.
    $captchas = $this->results->exp_captcha( $exp );

    //If $captchas has results.
    if ( !empty( $captchas ) ) {
      //Loop all captchas to delete captcha images from /pub/img/captcha/.
      foreach ( $captchas as $captcha ) {
        if ( file_exists( './pub/img/captcha/'.$captcha->captcha_time.'.jpg' ) ) {
          //Delete image files from captcha dir.
          unlink( './pub/img/captcha/'.$captcha->captcha_time.'.jpg' );
        }

        //Delete captcha record from database.
        $this->results->delete_captcha( $captcha->captcha_time );
      }
    }

    /**********************************
        Delete expired login attempts.
    ***********************************/
    $this->results->remove_exp_login_attempts( $exp );

    /***************
    * Cookie Check.
    ****************/
    if ( get_cookie( 'username' ) ) {
      //If username was found or not in the database.
      $verify_user = $this->results->username_check( get_cookie( 'username' ) );

      //If not found logout the user.
      if ( $verify_user == 0 ) {
        redirect( '/admincp/logout/' );
      } else {
        //Set user session.
        $session = array(
          'username' => get_cookie( 'username' ),
          'logged_in' => true
        );

        //Create Session.
        $this->session->set_userdata( $session );
      }
    }

    /****************
    * SESSION Check
    *****************/
    if ( $this->session->userdata( 'logged_in' ) ) {
      //Session username.
      $username = $this->session->userdata( 'username' );

      //If username was not found in the database.
      $verify_user = $this->results->username_check( $username );

      //If not found logout the user.
      if ( $verify_user == 0 ) {
        redirect( '/admincp/logout/' );
      }
    }
  }

  //Check if user reached max attempts failed login.
  private function check_attempts() {
    //Max Attempts.
    $max = 5;

    //User IP Address.
    $ip = $this->input->ip_address();

    //User's number of attempts.
    $attempts = $this->results->count_login_attempts( $ip );

    //If attempts reached max.
    if ( $attempts >= $max ) {
      return true;
    }

    return false;
  }

  public function login() {
    //Check if request is POST.
    if ( $_POST ) {
      //Username input.
      $username = $this->input->post( 'username' );

      //Password input.
      $password = $this->input->post( 'password' );

      //Rememberme value from post request.
      $rememberme = $this->input->post( 'rememberme' );

      //Check if there's no login failed attempts.
      if ( !$this->check_attempts() ) {
        /*************************
        * Setup Form Validation.
        *************************/
        //From Validation for username.
        $this->form_validation->set_rules( 'username', 'Username', 'callback_check_username' );

        //Form Validation for Password.
        $this->form_validation->set_rules( 'password', 'Password', 'callback_login_verify' );

        //Initiate Form Validation. If fails create attempt record to the database.
        if ( $this->form_validation->run() == false ) {
          //Failed attempt values.
          $attempt = array(
            'id' => '',
            'ip' => $this->input->ip_address(),
            'exp' => microtime(true)
          );

          //Insert Failed Attempts to database.
          $this->results->insert_attempts( $attempt );
        } else {
          //Remove User's Failed Login Attempts from the database.
          $this->results->remove_login_attempts( $this->input->ip_address() );

          //If Remember Me is Checked.
          if ( $rememberme && $rememberme == 1 ) {
            //Set Cookie for remembering username.
            set_cookie( 'username', $username, 9587454254 );
          }

          //Set user session.
          $session = array(
            'username' => $username,
            'logged_in' => true
          );

          //Create Session.
          $this->session->set_userdata( $session );

          //Redirect to dashboard page.
          redirect( '/admincp/dashboard/' );
        }
      } else {
        /*************************
        * Setup Form Validation.
        *************************/
        //Form Validation for Username.
        $this->form_validation->set_rules( 'username', 'Username', 'callback_check_username' );

        //Form Validation for Password.
        $this->form_validation->set_rules( 'password', 'Password', 'callback_login_verify' );

        //Form Validation for Captcha.
        $this->form_validation->set_rules( 'captcha', 'Captcha', 'callback_captch_check' );

        //Initiate Form Validation. And if login success and no errors.
        if ( $this->form_validation->run() ) {
          //Remove User's Failed Login Attempts from the database.
          $this->results->remove_login_attempts( $this->input->ip_address() );

          //If Remember Me is Checked.
          if ( $rememberme && $rememberme == 1 ) {
            //Set Cookie for remembering username.
            set_cookie( 'username', $username, 9587454254 );
          }

          //Set user session.
          $session = array(
            'username' => $username,
            'logged_in' => true
          );

          //Create Session.
          $this->session->set_userdata( $session );

          //Redirect to dashboard page.
          redirect( '/admincp/dashboard/' );
        }
      }
    }

    /*******************
    * Login Main View.
    *******************/
    //Header Title.
    $default['title'] = "Administrator Login - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical.'admincp/login/';

    //Page view name.
    $default['page'] = 'login';

    //Check if user reached max attempts. captcha will be required to login.
    $default['attempts'] = $this->check_attempts();

    //Load View For This Page.
    $this->load->view('admin/body/body', $default);
  }

  //Username Validation.
  public function check_username( $username ) {
    //If username field is empty.
    if ( $username == '' ) {
      $this->form_validation->set_message( 'check_username', 'Username field is required.' );
      return false;
    } else {
      //Pass data to model.
      $u = $this->results->username_check( $username );

      if ( $u == 1 ) {
        return true;
      } else if ( $u == 0 ) {
        $this->form_validation->set_message( 'check_username', '' );
        return false;
      }
    }

    return false;
  }

  //Check username and password if matched and Password validation.
  public function login_verify() {
    //Username value from post request.
    $username = $this->input->post( 'username' );

    //Password value from post request.
    $password = $this->input->post( 'password' );

    //If password field is empty.
    if ( $password == '' ) {
      //Set error message.
      $this->form_validation->set_message( 'login_verify', 'Password field is required.' );

      return false;
    } else {
      //Verify if username and password matches.
      $verify = $this->results->chech_login_details( $username, $this->hash( $password ) );

      //Verification failed or success.
      if ( $verify == 0 ) {
        //Set Error Message.
        $this->form_validation->set_message( 'login_verify', 'Username and Password did not match.' );

        return false;
      } else if ( $verify == 1 ) {
        return true;
      }
    }

    return false;
  }

  //Check captcha is valid.
  public function captch_check( $captcha ) {
    //If captcha field is empty.
    if ( $captcha == '' ) {
      $this->form_validation->set_message( 'captch_check', 'Captcha field is required.' );

      return false;
    } else {
      //Verify if captcha is valid.
      $verify = $this->results->check_captcha( $captcha );

      //Check captcha if failed or success.
      if ( $verify == 0 ) {
        //Set Error Message.
        $this->form_validation->set_message( 'captch_check', 'Image Captcha does not match.' );

        return false;
      } else {
        return true;
      }
    }

    return false;
  }

  //Password Hashing.
  private function hash( $password ) {
    //Salting Password.
    $pass = $password.''.$this->salt;

    //Encrypt to md5 and return.
    return md5( $pass );
  }

  //Logout
  public function logout() {
    //If has Cookie delete it.
    if ( get_cookie( 'username' ) ) {
      delete_cookie( 'username' );
    }

    //Destroy Session.
    $this->session->sess_destroy();

    //Redirect to login page.
    redirect( '/admincp/login/' );
  }

  //Main Page.
  public function index() {
    if ( $this->session->userdata( 'logged_in' ) ) {
      redirect( '/admincp/dashboard/' );
    } else {
      redirect( '/admincp/login/' );
    }
  }

  //Dashboard Page.
  public function dashboard() {
    //If has SESSION.
    if ( $this->session->userdata( 'logged_in' ) ) {
      //Header Title.
      $default['title'] = "Dashboard - Textmate.tk";

      //Page Canonical.
      $default['canonical'] = $this->canonical.'admincp/dashboard/';

      //Page view name.
      $default['page'] = 'dashboard';

      //For New Post Widget.
      $new_post = $this->results->get_new_post();

      //Inject new post to view.
      $default['new_post'] = $new_post;

      //Statistic Widget. Get Page Loaded Count.
      $hit_counts = $this->results->get_stats_count();

      //Statistics Widget. Get Unique Counts.
      $hit_unique = $this->results->basic( 'SELECT DISTINCT `ip` FROM `statistics` GROUP BY `ip` ORDER BY COUNT(*) DESC' );

      //Get Globe Textmate Count and Inject to view.
      $default['globe_count'] = $this->results->cat_counts( 'Globe Textmate' );

      //Get Smart Textmate Count and Inject to view.
      $default['smart_count'] = $this->results->cat_counts( 'Smart Textmate' );

      //Get Sun Textmate Count and Inject to view.
      $default['sun_count'] = $this->results->cat_counts( 'Sun Textmate' );

      //Get TM Textmate Count and Inject to view.
      $default['tm_count'] = $this->results->cat_counts( 'TM Textmate' );

      //Get TNT Textmate Count and Inject to view.
      $default['tnt_count'] = $this->results->cat_counts( 'TNT Textmate' );

      //Get Red Textmate Cout and Inject to view.
      $default['red_count'] = $this->results->cat_counts( 'Red Textmate' );

      //Get Text Clan Count and Inject to view.
      $default['clan_count'] = $this->results->cat_counts( 'Text Clan' );

      //Get Text Quotes Count and Inject to view.
      $default['quotes_count'] = $this->results->cat_counts( 'Text Quotes' );

      //Get Relationship Count and Inject to view.
      $default['rel_count'] = $this->results->cat_counts( 'Relationship' );

      //Inject Hit Counts to the view.
      $default['hit_counts'] = $hit_counts;

      //Inject Unique Counts to the view.
      $default['hit_unique'] = $hit_unique;

      //Search Logs get contents.
      $keywords = file_get_contents( './pub/logs/keywords.json' );

      //Decode JSON to Array.
      $keywords = json_decode( $keywords );

      //Inject Keywords array to the view.
      $default['keywords'] = $keywords;

      //Load View For This Page.
      $this->load->view('admin/body/body', $default);
    } else {
      redirect( '/admincp/login/' );
    }
  }

  /**********************
  * Textmate Posts Page.
  ***********************/
  public function posts() {
    //If has SESSION.
    if ( $this->session->userdata( 'logged_in' ) ) {
      //Header Title.
      $default['title'] = "Messages - Textmate.tk";

      //Page Canonical.
      $default['canonical'] = $this->canonical.'admincp/posts/';

      //Page view name.
      $default['page'] = 'posts';

      //Load View For This Page.
      $this->load->view('admin/body/body', $default);
    } else {
      redirect( '/admincp/login/' );
    }
  }

  /*************
  * Logs Page.
  **************/
  public function logs() {
    //If has SESSION.
    if ( $this->session->userdata( 'logged_in' ) ) {
      //Header Title.
      $default['title'] = "Logs - Textmate.tk";

      //Page Canonical.
      $default['canonical'] = $this->canonical.'admincp/logs/';

      //Page view name.
      $default['page'] = 'logs';

      /***********************
      * Search Keywords Logs.
      ************************/
      //If keywords.json is exists.
      if ( file_exists( './pub/logs/keywords.json' ) ) {
        //Get keywords.json contents.
        $keywords = file_get_contents( './pub/logs/keywords.json' );

        //JSON to Array.
        $keywords = json_decode( $keywords );

        //Inject $keywords to the view.
        $default['keywords'] = $keywords;
      } else {
        $keywords = '';
      }

      //Load View For This Page.
      $this->load->view('admin/body/body', $default);
    } else {
      redirect( '/admincp/login/' );
    }
  }

  /*****************
  * Statistics Page
  ******************/
  public function statistics() {
    //If has SESSION.
    if ( $this->session->userdata( 'logged_in' ) ) {
      //Header Title.
      $default['title'] = "Statistics - Textmate.tk";

      //Page Canonical.
      $default['canonical'] = $this->canonical.'admincp/statistics/';

      //Page view name.
      $default['page'] = 'statistics';

      //Total Page Load Count.
      $page_load_count = $this->results->get_page_load_count();

      //Total Unique Visitor's Count.
      $unique_count = $this->results->basic( 'SELECT DISTINCT `ip` FROM `statistics` GROUP BY `ip` ORDER BY COUNT(*) DESC' );

      //Inject Total Page Load Count to the view.
      $default['page_load_count'] = $page_load_count;

      //Inject Total Unique Visitor's Count to the view.
      $default['unique_count'] = $unique_count;

      //Load View For This Page.
      $this->load->view('admin/body/body', $default);
    } else {
      redirect( '/admincp/login/' );
    }
  }

  /*********************
  * Account Panel Page.
  **********************/
  public function account_panel() {
    //If has SESSION.
    if ( $this->session->userdata( 'logged_in' ) ) {
      //Header Title.
      $default['title'] = "Statistics - Textmate.tk";

      //Page Canonical.
      $default['canonical'] = $this->canonical.'admincp/account_panel/';

      //Page view name.
      $default['page'] = 'account_panel';

      //List of Administrator accounts.
      $admins = $this->results->get_admin_list();

      //Inject admin list to the view.
      $default['admins'] = $admins;

      //Load View For This Page.
      $this->load->view('admin/body/body', $default);
    } else {
      redirect( '/admincp/login/' );
    }
  }

  /**********************
  * Change Password Page.
  ***********************/
  public function change_password() {
    //If has SESSION.
    if ( $this->session->userdata( 'logged_in' ) ) {
      //If has success notification.
      $default['success'] = false;

      //If has post request.
      if ( $_POST ) {
        //Current Password Post Request Value.
        $current_password = $this->input->post( 'current_password' );

        //New Password Post Request Value.
        $new_password = $this->input->post( 'new_password' );

        //Confirm New Password Post Request.
        $confirm_password = $this->input->post( 'confirm_password' );

        /******************
        * Form Validation.
        *******************/
        //Form validation for $current_password.
        $this->form_validation->set_rules( 'current_password', 'Current Password', 'callback_current_password_check' );

        //Form validation for $new_password.
        $this->form_validation->set_rules( 'new_password', 'New Password', 'callback_new_password_check' );

        //Form validation for $confirm_password.
        $this->form_validation->set_rules( 'confirm_password', 'Confirm Password', 'callback_confirm_password_check' );

        //Initiate Form Validation.
        if ( $this->form_validation->run() ) {
          //Set data to update password.
          $data = array(
            'password' => $this->hash( $new_password )
          );

          //Update Password.
          if ( $this->results->update_password( $data, $this->session->userdata( 'username' ) ) ) {
            //If has success notification.
            $default['success'] = true;
          }
        }
      }

      //Header Title.
      $default['title'] = "Change Password - Textmate.tk";

      //Page Canonical.
      $default['canonical'] = $this->canonical.'admincp/change_password/';

      //Page view name.
      $default['page'] = 'change_password';

      //Load View For This Page.
      $this->load->view('admin/body/body', $default);

    } else {
      redirect( '/admincp/login/' );
    }
  }

  //Check current password.
  public function current_password_check( $current_password ) {
    //Admin Current Password.
    $c_password = $this->results->get_admin_password( $this->session->userdata( 'username' ) );

    //Matching password.
    if ( $current_password == '' ) {
      //Set Validation error message.
      $this->form_validation->set_message( 'current_password_check', 'Current Password field is required.' );

      return false;
    } else if ( $this->hash( $current_password ) !== $c_password ) {
      //Set Validation error message.
      $this->form_validation->set_message( 'current_password_check', 'Invalid Current Password.' );

      return false;
    } else {
      return true;
    }
  }

  //Check new password.
  public function new_password_check( $new_password ) {
    //Check if new_password is empty or not.
    if ( $new_password !== '' ) {
      return true;
    } else {
      //Set Validation error message.
      $this->form_validation->set_message( 'new_password_check', 'New Password is required.' );

      return false;
    }
  }

  //Confirm new_password and confirm_password is matched.
  public function confirm_password_check() {
    //New Password Post Request Value.
    $new_password = $this->input->post( 'new_password' );

    //Confirm New Password Post Request.
    $confirm_password = $this->input->post( 'confirm_password' );

    //Check new_password and confirm_password is match.
    if ( $confirm_password == '' ) {
      //Set Validation error message.
      $this->form_validation->set_message( 'confirm_password_check', 'Confirm New Password is required.' );

      return false;
    } else if ( $new_password !== $confirm_password ) {
      //Set Validation error message.
      $this->form_validation->set_message( 'confirm_password_check', 'New Password and Confirm Password does not match.' );

      return false;
    } else {
      return true;
    }
  }
}
