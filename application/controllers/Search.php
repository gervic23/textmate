<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
  private $canonical;

  public function __construct() {
    //Load Parent CI Controller Constructor.
    parent::__construct();

    //Default Canonical.
    $this->canonical = base_url();

    //Load HTML Helper.
    $this->load->helper( 'html' );

    //Include model.
    $this->load->model('results');

    //Load Form Helper
    $this->load->helper( 'form' );

    //Load Form validation library.
    $this->load->library( 'form_validation' );

    //Load File Helper.
    $this->load->helper( 'file' );
  }

  public function index() {
    //Check if is POST Request.
    if ( $_POST ) {

      //Header Title.
      $default['title'] = "Looking For Textmate? - Textmate.tk";

      //Page Canonical.
      $default['canonical'] = $this->canonical;

      //Page view name.
      $default['page'] = 'search';

      //Get Request Value of "s".
      $key_w = $this->security->xss_clean( $this->input->post( 's' ) );

      //Inject this value to the view.
      $default['category'] = $key_w;

      /***********************
      * Search Keywords Logs.
      ************************/
      //Get the contents of json file.
      $logs = file_get_contents( './pub/logs/keywords.json' );

      //if file content is empty.
      if ( empty( $logs ) ) {
        //Set array values for logs.
        $data['logs'][] = array(
          'keywords' => $key_w,
          'ip_address' => $this->input->ip_address(),
          'date' => date( "Y-m-d H:i:s" )
        );

        //Convert Array to JSON.
        $json = json_encode( $data );

        //Write a content to keywords.json
        write_file( './pub/logs/keywords.json', $json, 'w' );
      } else {
        //Convert JSON to php Array.
        $data = json_decode( $logs );

        //Get Logs Values.
        $old = $data->logs;

        //Set new Array Values to Append.
        $new[] = array(
          'keywords' => $key_w,
          'ip_address' => $this->input->ip_address(),
          'date' => date( "Y-m-d H:i:s" )
        );

        //Merge old array and new as one (Append Arrays).
        $am['logs'] = array_merge( $old, $new );

        //Convert Array to JSON
        $json = json_encode( $am );

        //Write a content to keywords.json
        write_file( './pub/logs/keywords.json', $json, 'w' );
      }


      //Explode keywords via spaces.
      $k = explode(' ', $key_w);

      //Result Count.
      $default['count'] = $this->results->search_count( $k );

      //Get Search Posts Results.
      $default['messages'] = $this->results->search_posts( $k );

      //Setup form validation rules.
      $this->form_validation->set_rules( 's', 'Search', 'required|min_length[3]', array(
        'required' => 'Please type your keywords.',
        'min_length' => 'Keywords too short.'
      ) );

      //Initiate form validation.
      if ( $this->form_validation->run() == false ) {
        //Set boolean if there is a validation error.
        $default['has_validation'] = true;

        //if search field is empty put something on the h1 view.
         if ( strlen( $key_w ) == 0 ) {
           $default['category'] = 'No Keywords';
         }
      } else {
        //Set boolean if there is a validation error.
        $default['has_validation'] = false;
      }

      $this->load->view('textmate/body/body', $default);
    } else {
      redirect('/');
    }
  }
}
