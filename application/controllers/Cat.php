<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cat extends CI_Controller {
  private $canonical;

  public function __construct() {
    //Load Parent CI Controller Constructor.
    parent::__construct();

    //Default Canonical.
    $this->canonical = base_url();

    //Load HTML Helper.
    $this->load->helper( 'html' );

    //Include model.
    $this->load->model( 'results' );

    //Load Form Helper
    $this->load->helper( 'form' );
  }

  public function index() {
    redirect(base_url());
  }

  public function globe() {
    //Category.
    $category = 'Globe Textmate';

    //Header Title.
    $default['title'] = "Looking For Globe Textmate? - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical.'globe/';

    //Page view name.
    $default['page'] = 'cat';

    //Inject this value to the view.
    $default['category'] = $category;

    //Get Globe Posts from the Database.
    $default['messages'] = $this->results->posts( $category );

    $this->load->view('textmate/body/body', $default);
  }

  public function smart() {
    $category = 'Smart Textmate';

    //Header Title.
    $default['title'] = "Looking For Smart Textmate? - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical.'smart/';

    //Page view name.
    $default['page'] = 'cat';

    //Inject this value to the view.
    $default['category'] = $category;

    //Get Globe Posts from the Database.
    $default['messages'] = $this->results->posts( $category );

    $this->load->view('textmate/body/body', $default);
  }

  public function sun() {
    $category = 'Sun Textmate';

    //Header Title.
    $default['title'] = "Looking For Sun Textmate? - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical.'sun/';

    //Page view name.
    $default['page'] = 'cat';

    //Inject this value to the view.
    $default['category'] = $category;

    //Get Globe Posts from the Database.
    $default['messages'] = $this->results->posts( $category );

    $this->load->view('textmate/body/body', $default);
  }

  public function tm() {
    $category = 'TM Textmate';

    //Header Title.
    $default['title'] = "Looking For TM Textmate? - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical.'tm/';

    //Page view name.
    $default['page'] = 'cat';

    //Inject this value to the view.
    $default['category'] = $category;

    //Get Globe Posts from the Database.
    $default['messages'] = $this->results->posts( $category );

    $this->load->view('textmate/body/body', $default);
  }

  public function tnt() {
    $category = 'TNT Textmate';

    //Header Title.
    $default['title'] = "Looking For TNT Textmate? - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical.'tnt/';

    //Page view name.
    $default['page'] = 'cat';

    //Inject this value to the view.
    $default['category'] = $category;

    //Get Globe Posts from the Database.
    $default['messages'] = $this->results->posts( $category );

    $this->load->view('textmate/body/body', $default);
  }

  public function red() {
    $category = 'Red Textmate';

    //Header Title.
    $default['title'] = "Looking For Red Textmate? - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical;

    //Page view name.
    $default['page'] = 'cat';

    //Inject this value to the view.
    $default['category'] = $category;

    //Get Globe Posts from the Database.
    $default['messages'] = $this->results->posts( $category );

    $this->load->view('textmate/body/body', $default);
  }

  public function text_clan() {
    $category = 'Text Clan';

    //Header Title.
    $default['title'] = "Looking For Smart Textmate? - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical.'text_clan/';

    //Page view name.
    $default['page'] = 'cat';

    //Inject this value to the view.
    $default['category'] = $category;

    //Get Globe Posts from the Database.
    $default['messages'] = $this->results->posts( $category );

    $this->load->view('textmate/body/body', $default);
  }

  public function quotes() {
    $category = 'Text Quotes';

    //Header Title.
    $default['title'] = "Looking For Smart Textmate? - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical.'quotes/';

    //Page view name.
    $default['page'] = 'cat';

    //Inject this value to the view.
    $default['category'] = $category;

    //Get Globe Posts from the Database.
    $default['messages'] = $this->results->posts( $category );

    $this->load->view('textmate/body/body', $default);
  }

  public function relationship() {
    $category = 'Relationship';

    //Header Title.
    $default['title'] = "Looking For Smart Textmate? - Textmate.tk";

    //Page Canonical.
    $default['canonical'] = $this->canonical.'relationsip/';

    //Page view name.
    $default['page'] = 'cat';

    //Inject this value to the view.
    $default['category'] = $category;

    //Get Globe Posts from the Database.
    $default['messages'] = $this->results->posts( $category );

    $this->load->view('textmate/body/body', $default);
  }
}
