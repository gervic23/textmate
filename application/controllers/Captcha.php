<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Captcha extends CI_Controller {
  public function __construct() {
    //Load Main Constructor.
    parent::__construct();

    //Load Model.
    $this->load->model( 'results' );

    //Load Captcha Helper.
    $this->load->helper( 'captcha' );
  }

  public function index() {
    //Users IP Address.
    $ip = $this->input->ip_address();

    //Set Captcha Values.
    $val = array(
      'img_path' => './pub/img/captcha/',
      'img_url' => base_url().'pub/img/captcha/',
      'font_path' => './pub/fonts/calibrii.ttf',
      'img_width' => '300',
      'img_height' => '70',
      'expiration' => 3600
    );

    //Create Captcha
    $cap = create_captcha( $val );
    echo $cap['image'];

    $data = array(
      'id' => '',
      'captcha_time' => $cap['time'],
      'ip' => $ip,
      'word' => $cap['word']
    );

    $this->results->insert_captcha( $data );
  }
}
