<?=doctype(); ?>
<html lang="en-US" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title><?php echo $title; ?></title>
  <link href="/pub/img/mytextmate-icon.png" rel="shortcut icon" />
  <link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Aldrich' rel='stylesheet'>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
  <link rel="canonical" href="<?php echo $canonical; ?>" />
  <link rel="stylesheet" href="/pub/css/style.css" />
  <link rel="stylesheet" href="/pub/css/stars.css" />
  <link rel="stylesheet" href="/pub/css/animate.css" />
  <script type="text/javascript" src="/pub/js/jquery-3.1.1.min.js"></script>
  <script type="text/javascript" src="/pub/js/tracker.js"></script>
  <script type="text/javascript" src="/pub/js/jquery.visible.min.js"></script>
  <script type="text/javascript">console.log("%cThere are nothing to see here.", "color: blue; font-size: 20px;");</script>
  <?php if ( $this->uri->segment(1) == 'home' || $this->uri->segment(1) == '' ): ?>
    <script type="text/javascript" src="/pub/js/textition.js"></script>
  <?php endif; ?>
</head>
  <body>
    <header class="main-header">
      <a href="/"><img alt="logo" class="logo" src="/pub/img/mytextmate-icon.png" /></a>
      <nav>
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="/cat/globe/">Globe</a></li>
          <li><a href="/cat/smart/">Smart</a></li>
          <li><a href="/cat/sun/">Sun</a></li>
          <li><a href="/cat/tm/">TM</a></li>
          <li><a href="/cat/tnt/">TNT</a></li>
          <li><a href="/cat/red">Red</a></li>
          <li><a href="/cat/text_clan/">Text Clan</a></li>
          <li><a href="/cat/quotes/">Text Quotes</a></li>
          <li><a href="/cat/relationship/">Relationship</a></li>
        </ul>
      </nav>
      <img class="search-btn" alt="search-btn" src="/pub/img/search.png" />
      <div class="clear"></div>
      <div class="search-form-container">
        <?=form_open( 'search/', array( 'class' => 'search-form', 'method' => 'POST' ) ); ?>
          <?=form_input( array( 'name' => 's', 'class' => 'search_input', 'placeholder' => 'Type keywords here...', 'autocomplete' => 'off', 'maxlength' => '25' ) ); ?>
          <img class="search-btn-2" alt="search-btn-2" src="/pub/img/search-icon.png" />
        <?=form_close(); ?>
      </div>
    </header>

    <header class="mobile-header">
      <span class="mobile-menu menu"></span>
      <a href="/"><span class="image-logo"></span></a>
      <?=form_open( '/search/', array( 'class' => 'mobile-search-form', 'method' => 'POST' ) ); ?>
        <?=form_input( array( 'name' => 's', 'class' => 'search_input', 'placeholder' => 'Type keywords here...', 'autocomplete' => 'off', 'maxlength' => '25' ) ); ?>
        <img class="mobile-search-btn" alt="search-btn-2" src="/pub/img/search-icon.png" />
      <?=form_close(); ?>
    </header>

    <nav class="mobile-nav">
      <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/cat/globe/">Globe</a></li>
        <li><a href="/cat/smart/">Smart</a></li>
        <li><a href="/cat/sun/">Sun</a></li>
        <li><a href="/cat/tm/">TM</a></li>
        <li><a href="/cat/tnt/">TNT</a></li>
        <li><a href="/cat/red">Red</a></li>
        <li><a href="/cat/text_clan/">Text Clan</a></li>
        <li><a href="/cat/quotes/">Text Quotes</a></li>
        <li><a href="/cat/relationship/">Relationship</a></li>
      </ul>
      <div class="clear"></div>
    </nav>

    <script type="text/javascript">
      var search_bool = false;
      var nav_bool = false;

      $(".search-btn").on("click", function() {
        if ( search_bool == false ) {
          search_bool = true;
          $(".search-form-container").fadeIn();
        } else {
          search_bool = false;
          $(".search-form-container").fadeOut();
        }
      });

      $(".search-btn-2").on("click", function() {
        $(".search-form").submit();
      });

      $(".mobile-search-btn").on("click", function() {
        $(".mobile-search-form").submit();
      });

      $(".mobile-menu").on("click", function() {
        if ( nav_bool == false ) {
          nav_bool = true;

          $(".mobile-nav").animate({
            left: 0
          }, 1000);
        } else {
          nav_bool = false;

          $(".mobile-nav").animate({
            left: "-215px"
          }, 1000);
        }
      });
    </script>

    <div class="main-container">
      <?php if ( $this->uri->segment(1) == 'home' || $this->uri->segment(1) == '' ): ?>
        <div class="homepage-header">
          <div class="stars"></div>
          <div class="twinkling"></div>
          <div class="clouds"></div>
          <div class="clear"></div>
          <div class="textition-container">
            <p>Looking For Textmate?</p>
            <p>Textmate.tk</p>
          </div>
        </div>
        <script type="text/javascript">
          //Initiate Method.
          screen_size();
          textition_position();

          //If window size change.
          $(window).resize(function() {
            screen_size();
            textition_position();
          });

          $('.textition-container').textition({
        		 speed: 1,
        		 animation: 'ease-out',
        		 map: {x: 200, y: 100, z: 0},
        		 autoplay: true,
        		 interval: 5
        	});

          //Method for to get window size and set .homepage-header responsive size.
          function screen_size() {
            //Current window size.
            var current_height = $(window).height();

            //Set .homepage-header size equal to window size.
            $(".homepage-header").css("height", current_height + "px");
          }

          function textition_position() {
            var screen_width = $(window).width() - 17;

            $(".textition-container").css({
              "top": 49 + "%",
              "width": screen_width + "px"
            });
          }
        </script>
      <?php endif; ?>
      <div class="main-contents">
        <?php $this->load->view('textmate/contents/'.$page); ?>
      </div>
    </div>
    <footer class="main-footer">
      Textmate Created by <a target="_blank" href="https://www.facebook.com/gervic23">Gervic</a>. copyright &copy; <?php echo date("Y"); ?>
    </footer>
  </body>
</html>
