<?php
  $csrf = array(
    'name' => $this->security->get_csrf_token_name(),
    'hash' => $this->security->get_csrf_hash()
  );

  $form_name = array(
    'id' => 'name',
    'class' => 'form_input',
    'autocomplete' => 'off',
    'placeholder' => 'Type your name here.',
    'maxlength' => 20
  );

  $form_gender = array(
    '' => 'Select Gender',
    'Male' => 'Male',
    'Female' => 'Female'
  );

  $form_message = array(
    'id' => 'message',
    'class' => 'form_textarea',
    'placeholder' => 'Type your message here.',
    'maxlength' => 500
  );
?>

<div class="cat-container">
  <div class="cat-ads">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- Textmate-Ads -->
    <ins class="adsbygoogle"
         style="display:block"
         data-ad-client="ca-pub-0303989800108633"
         data-ad-slot="7208494827"
         data-ad-format="auto"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
  </div>
  <div class="message-form">
    <div class="post-button post-btn">Post Message</div>
    <div class="form">
      <?=form_input( $form_name ); ?> <?=form_dropdown( 'gender', $form_gender, '', array( 'id' => 'gender', 'class' => 'form_input' ) ); ?>
      <?=form_textarea( $form_message ); ?>
      <div class="message-len">Characters Remaining: <span>500</span>.</div>
      <div class="form-validation">&nbsp;</div>
      <div class="form_submit"><span class="post-button post-submit">Submit</span></div>
    </div>
  </div>
  <div class="cat-header">
    <h1><?php echo $category; ?></h1>
  </div>
  <div class="ajax-post"></div>
  <div class="post-messages">
    <ul>
      <?php foreach ( $messages as $message ): ?>
        <li class="post">
          <div class="message"><?php echo $message->message; ?></div>
          <ul>
            <li>Name: <?php echo $message->name; ?></li>
            <li>Gender: <?php echo $message->gender; ?></li>
            <li>Category: <?php echo $message->network; ?></li>
            <li>Date: <?php echo $message->date; ?></li>
          </ul>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
  <div class="ajax-loader-container">
    <img alt="ajax-loader" src="/pub/img/loading3.gif" />
  </div>
  <div class="read-more-container">
    <span class="post-button read-more-btn">Read More</span>
  </div>
</div>

<script type="text/javascript">
  var form_bool = false;
  var rows = 50;

  cat_size();

  // $(".post-messages ul li.post").addClass("hidden").viewportChecker({
  //   classToAdd: "visible animated bounceInLeft",
  //   offset: 100
  // });

  $(".post-messages ul li.post").each(function() {
    $(this).addClass("hidden");

    if ($(this).visible()) {
      $(this).addClass("visible").addClass("animated").addClass("bounceInLeft");
    }
  });

  $(window).on("scroll", function() {
    $(".post-messages ul li.post").each(function() {
      $(this).addClass("hidden");

      if ($(this).visible()) {
        $(this).addClass("visible").addClass("animated").addClass("bounceInLeft");
      }
    });
  });

  $(window).resize(function() {
    cat_size();
  });

  $(".post-btn").on("click", function() {
    if ( form_bool == false ) {
      form_bool = true;
      $(".message-form .form").fadeIn();
    } else {
      form_bool = false;
      $(".message-form .form").fadeOut();
    }
  });

  $(".read-more-btn").on("click", function() {
    $(this).hide();
    $(".ajax-loader-container").show();

    rows = rows + 50;

    var data = {
      "rows": rows,
      "network": "<?php echo $category; ?>",
    };

    $.post("/ajax/more_results/", data, function( r ) {
      $(".ajax-loader-container").hide();
      $(".read-more-btn").show();

      $(".post-messages").html( r );
      console.log(r);
    });
  });

  $(".form_textarea").on("keyup", function() {
    var txt_len = $(".form_textarea").val().length;
    var current_len = 500 - txt_len;
    $(".message-len span").html( current_len );
  });

  $(".post-submit").on("click", function() {
    var name = $("#name").val();
    var gender = $("#gender").val();
    var message = $("#message").val();

    if ( name == "" ) {
      $(".form-validation").css("background-color", "#cc1a11").html( "Please provide your name." );
      $("#name").css({"border": 2 + "px solid #cc1a11"});
    } else if ( gender == "" ) {
      $(".form-validation").css("background-color", "#cc1a11").html( "Please provide your gender." );
      $("#gender").css({"border": 2 + "px solid #cc1a11"});
    } else if ( message == "" ) {
      $(".form-validation").css("background-color", "#cc1a11").html( "Please provide your gender." );
      $("#message").css({"border": 2 + "px solid #cc1a11"});
    } else {
      $(".ajax-post").css({"text-align": "center"}).html("<img src='/pub/img/loading3.gif' />");

      if ( $(window).width() <= 500 ) {
        form_bool = false;
        $(".message-form .form").fadeOut();
      }

      var data = {
        "<?=$csrf['name']; ?>": "<?=$csrf['hash']; ?>",
        "name": name,
        "gender": gender,
        "message": message,
        "network": "<?php echo $category; ?>"
      };

      $.post("/ajax/post_message/", data, function( r ) {
        setTimeout(function() {
          $(".form_submit").hide();
          $(".ajax-post").css({"text-align": "left"}).html( r );
        }, 2000);
      });
    }
  });

  $("#name").on("keyup", function() {
    $(".form-validation").css("background-color", "transparent").html( "&nbsp;" );
    $("#name").css({"border": 1 + "px solid #ccc"});
  });

  $("#gender").on("change", function() {
    $(".form-validation").css("background-color", "transparent").html( "&nbsp;" );
    $("#gender").css({"border": 1 + "px solid #ccc"});
  });

  $("#message").on("keyup", function() {
    $(".form-validation").css("background-color", "transparent").html( "&nbsp;" );
    $("#message").css({"border": 1 + "px solid #ccc"});
  });

  function cat_size() {
    var h = $(".main-header").height() + 10;
    $(".cat-container").css({"margin-top": h + "px"});
  }
</script>
