<div class="notfound-container">
  <div class="notfound-ads">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- Textmate-Ads -->
    <ins class="adsbygoogle"
         style="display:block"
         data-ad-client="ca-pub-0303989800108633"
         data-ad-slot="7208494827"
         data-ad-format="auto"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
  </div>

  <div class="notfound-contents">
    <img src="/pub/img/404.png" />
    <div class="notfound-text">The page you request was not found.</div>
  </div>
</div>

<script type="text/javascript">
  notfound_resize();

  $(window).resize(function() {
    notfound_resize();
  });

  function notfound_resize() {
    var win_height = $(window).height();
    var new_height = win_height - 159;

    $(".notfound-container").css({
      "position": "relative",
      "height": new_height + "px"
    });
  }
</script>
