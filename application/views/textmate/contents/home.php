<div class="home-header">
  <h1>Categories</h1>
</div>

<div class="home-container">
  <ul>
    <li>
      <h1><a href="/cat/globe/">Globe Textmate</a></h1>
      <p>Total Posts: <?php echo $globe_num; ?>.</p>
      <p>Latest Post From:<br /> <?php echo $globe_user_name; ?>.
        <?php
          $globe_post = str_ireplace( '<br>', '', $globe_post );
        ?>
        <p><?php echo $globe_post; ?></p>
      </p>
    </li>
    <li>
      <h1><a href="/cat/smart/">Smart Textmate</a></h1>
      <p>Total Posts: <?php echo $smart_num; ?>.</p>
      <p>Latest Post From:<br /> <?php echo $smart_user_name; ?>.
        <?php
          $smart_post = str_ireplace( '<br>', '', $smart_post );
        ?>
        <p><?php echo $smart_post; ?></p>
      </p>
    </li>
    <li>
      <h1><a href="/cat/sun/">Sun Textmate</a></h1>
      <p>Total Posts: <?php echo $sun_num; ?>.</p>
      <p>Latest Post From:<br /> <?php echo $sun_user_name; ?>.
        <?php
          $sun_post = str_ireplace( '<br>', '', $sun_post );
        ?>
        <p><?php echo $sun_post; ?></p>
      </p>
    </li>
    <li>
      <h1><a href="/cat/tm/">TM Textmate</a></h1>
      <p>Total Posts: <?php echo $tm_num; ?>.</p>
      <p>Latest Post From:<br /> <?php echo $tm_user_name; ?>.
        <?php
          $tm_post = str_ireplace( '<br>', '', $tm_post );
        ?>
        <p><?php echo $tm_post; ?></p>
      </p>
    </li>
    <li>
      <h1><a href="/cat/tnt/">TNT Textmate</a></h1>
      <p>Total Posts: <?php echo $tnt_num; ?>.</p>
      <p>Latest Post From:<br /> <?php echo $tnt_user_name; ?>.
        <?php
          $tnt_post = str_ireplace( '<br>', '', $tnt_post );
        ?>
        <p><?php echo $tnt_post; ?></p>
      </p>
    </li>
    <li>
      <h1><a href="/cat/red/">Red Textmate</a></h1>
      <p>Total Posts: <?php echo $red_num; ?>.</p>
      <p>Latest Post From:<br /> <?php echo $red_user_name; ?>.
        <?php
          $red_post = str_ireplace( '<br>', '', $red_post );
        ?>
        <p><?php echo $red_post; ?></p>
      </p>
    </li>
    <li>
      <h1><a href="/cat/text_clan/">Text Clan</a></h1>
      <p>Total Posts: <?php echo $clan_num; ?>.</p>
      <p>Latest Post From:<br /> <?php echo $clan_user_name; ?>.
        <?php
          $clan_post = str_ireplace( '<br>', '', $clan_post );
        ?>
        <p><?php echo $clan_post; ?></p>
      </p>
    </li>
    <li>
      <h1><a href="/cat/quotes/">Text Quotes</a></h1>
      <p>Total Posts: <?php echo $quotes_num; ?>.</p>
      <p>Latest Post From:<br /> <?php echo $quotes_user_name; ?>.
        <?php
          $quotes_post = str_ireplace( '<br>', '', $quotes_post );
        ?>
        <p><?php echo $quotes_post; ?></p>
      </p>
    </li>
    <li>
      <h1><a href="/cat/relationship/">Relationship</a></h1>
      <p>Total Posts: <?php echo $rel_num; ?>.</p>
      <p>Latest Post From:<br /> <?php echo $rel_user_name; ?>.
        <?php
          $rel_post = str_ireplace( '<br>', '', $rel_post );
        ?>
        <p><?php echo $rel_post; ?></p>
      </p>
    </li>
  </ul>
</div>
<div class="clear"></div>

<div class="home-ads">
  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <!-- Textmate-Ads -->
  <ins class="adsbygoogle"
       style="display:block"
       data-ad-client="ca-pub-0303989800108633"
       data-ad-slot="7208494827"
       data-ad-format="auto"></ins>
  <script>
  (adsbygoogle = window.adsbygoogle || []).push({});
  </script>
</div>

<script type="text/javascript">
  // $(".home-container ul li").addClass("hidden").viewportChecker({
  //   classToAdd: "visible animated bounceInUp",
  //   offset: 100
  // });

  $(".home-container ul li").each(function() {
    $(this).addClass("hidden");
  });

  $(window).on("scroll", function() {
    $(".home-container ul li").each(function() {
      if ($(this).visible()) {
        $(this).addClass("visible").addClass("animated").addClass("bounceInUp");
      }
    });
  });
</script>
