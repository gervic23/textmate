<?php
  $csrf = array(
    'name' => $this->security->get_csrf_token_name(),
    'hash' => $this->security->get_csrf_hash()
  );
?>
<?php if ( $count !== 0 && $has_validation == false ): ?>
  <div class="cat-container search-header">
    <div class="cat-header">
      <h1><?php echo $category; ?></h1>
    </div>
    <div class="search-count">You have <?php echo $count; ?> results found.</div>
    <div class="search-ads">
      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <!-- Textmate-Ads -->
      <ins class="adsbygoogle"
           style="display:block"
           data-ad-client="ca-pub-0303989800108633"
           data-ad-slot="7208494827"
           data-ad-format="auto"></ins>
      <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
    </div>
    <div class="post-messages">
      <ul>
        <?php foreach ( $messages as $message ): ?>
          <li class="post">
            <div class="message"><?php echo $message->message; ?></div>
            <ul>
              <li>Name: <?php echo $message->name; ?></li>
              <li>Gender: <?php echo $message->gender; ?></li>
              <li>Category: <?php echo $message->network; ?></li>
              <li>Date: <?php echo $message->date; ?></li>
            </ul>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
    <div class="ajax-loader-container">
      <img alt="ajax-loader" src="/pub/img/loading3.gif" />
    </div>
    <div class="read-more-container">
      <span class="post-button read-more-btn">Read More</span>
    </div>
  </div>

  <script type="text/javascript">
    var rows = 50;

    // $(".post-messages ul li.post").addClass("hidden").viewportChecker({
    //   classToAdd: "visible animated bounceInLeft",
    //   offset: 100
    // });

    $(".post-messages ul li.post").each(function() {
      $(this).addClass("hidden");

      if ($(this).visible()) {
        $(this).addClass("visible").addClass("animated").addClass("bounceInLeft");
      }
    });

    $(window).on("scroll", function() {
      $(".post-messages ul li.post").each(function() {
        $(this).addClass("hidden");

        if ($(this).visible()) {
          $(this).addClass("visible").addClass("animated").addClass("bounceInLeft");
        }
      });
    });

    $(".read-more-btn").on("click", function() {
      $(this).hide();
      $(".ajax-loader-container").show();

      rows = rows + 50;

      var data = {
        "rows": rows,
        "keywords": "<?php echo $category; ?>",
      };

      $.post("/ajax/search_more_results/", data, function( r ) {
        $(".ajax-loader-container").hide();
        $(".read-more-btn").show();

        $(".post-messages").html( r );
      });
    });
  </script>
<?php else: ?>
  <div class="cat-container search-header">
    <div class="cat-header">
      <h1><?php echo $category; ?></h1>
    </div>
    <div class="search-count">You have 0 results found.</div>
    <div class="search-ads">
      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <!-- Textmate-Ads -->
      <ins class="adsbygoogle"
           style="display:block"
           data-ad-client="ca-pub-0303989800108633"
           data-ad-slot="7208494827"
           data-ad-format="auto"></ins>
      <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
    </div>
    <?php if ( $has_validation ): ?>
      <div class="validation-errors">
        <?php echo validation_errors(); ?>
      </div>
    <?php endif; ?>
    <div class="post-messages">
      &nbsp;
    </div>
  </div>

  <script type="text/javascript">
    resize_empty_post_message();

    $(window).resize(function() {
      resize_empty_post_message();
    });

    function resize_empty_post_message() {
      var win_height = $(window).height();
      var new_height = win_height - 252;

      $(".post-messages").css({
        "position": "relative",
        "height": new_height + "px"
      });
    }
  </script>
<?php endif; ?>
