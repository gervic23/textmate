<div class="dashboard-container">
  <ul>
    <li class="widget post-feedback">
      <h1>New Posts</h1>
      <div class="posts-contents">
        <ul>
          <li>From: <?=$new_post->name; ?></li>
          <li>Post Date: <?=$new_post->date; ?></li>
          <li>Category: <?=$new_post->network; ?></li>
          <?php
            $new_msg = str_ireplace( '<br>', '', $new_post->message );
          ?>
          <li>
            <?php echo ( strlen( $new_msg ) >= 50 ? substr( $new_msg, 0, 50 ).'...' : $new_msg ); ?>
          </li>
        </ul>
        <div class="posts-count-contents">
          <h2>Categories Count</h2>
          <ul>
            <li>Globe Textmate: <?=$globe_count; ?></li>
            <li>Smart Textmate: <?=$smart_count; ?></li>
            <li>Sun Textmate: <?=$sun_count; ?></li>
            <li>TM Textmate: <?=$tm_count; ?></li>
            <li>TNT Textmate: <?=$tnt_count; ?></li>
            <li>Red Textmate: <?=$red_count; ?></li>
            <li>Text Clan: <?=$clan_count; ?></li>
            <li>Text Quotes: <?=$quotes_count; ?></li>
            <li>Relationship: <?=$rel_count; ?></li>
          </ul>
        </div>
      </div>
    </li>
    <li class="widget dashboard-stats-container">
      <h1>Statistics</h1>
      <table>
        <thead>
          <tr>
            <th>Page Loaded</th>
            <th>Unique Hits</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><?=$hit_counts; ?></td>
            <td><?=$hit_unique; ?></td>
          </tr>
        </tbody>
      </table>
    </li>
    <li class="widget">
      <h1>Logs</h1>
      <?php if ( $keywords ): ?>
        <div class="dashboard-search-contents">
          <h2>Search Keywords</h2>
          <?php $keywords = $keywords->logs; krsort( $keywords ); ?>
          <ul>
            <?php $i = 0; foreach( $keywords as $keyword ): $i++; ?>
              <?php if ( $i <= 2 ): ?>
                <li class="list">
                  <ul>
                    <li>Keywords: <?=$keyword->keywords; ?></li>
                    <li>IP Address: <?=$keyword->ip_address; ?></li>
                    <li>Date: <?=$keyword->date; ?></li>
                  </ul>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>
        </div>
      <?php endif; ?>
    </li>
  </ul>
  <div class="clear"></div>
</div>
