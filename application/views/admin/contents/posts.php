<div class="posts-container">
  <div class="ajax-post"></div>
</div>

<script type="text/javascript">
  var ajax_scroll_height = 0;
  resize_ajax_post();

  $(window).resize(function() {
    resize_ajax_post();
  });

  $(".ajax-post").load( "/ajax/ajax_posts/" );

  function resize_ajax_post() {
    var win_height = $(window).height();
    var new_height = win_height - 200;

    $(".ajax-post").css({
      "height": new_height + "px"
    });
  }
</script>
