<div class="account-panel-container">
  <div class="account-panel-admin-list">
    <ul>
      <?php foreach ( $admins as $admin ): ?>
        <li class="list">
          <div class="left">
            <?=$admin->username; ?>
          </div>
          <div class="right"><span class="admin-del" onclick="javascript: del_admin( <?=$admin->id; ?> )">Delete</span></div>
          <div class="clear"></div>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>

<script type="text/javascript">
  function del_admin( id ) {
    var data = { "id": id };

    $.post("/ajax/del_admin/", data, function( r ) {
      alert( r );
    });
  }
</script>
