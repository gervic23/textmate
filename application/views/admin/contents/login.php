<?php
  $csrf = array(
    'name' => $this->security->get_csrf_token_name(),
    'hash' => $this->security->get_csrf_hash()
  );
?>

<?php if ( validation_errors() ): ?>
<div class="validation-errors">
  <?php echo validation_errors(); ?>
</div>
<?php endif; ?>
<div class="login-container">
  <h1>Administrator Login</h1>
  <?=form_open( '/admincp/login', array( 'class' => 'login-form' ) ); ?>
    <div class="input-container">
      <?=form_input( array( 'name' => 'username', 'class' => 'form-input', 'placeholder' => 'Username', 'autocomplete' => 'off' ) ); ?>
    </div>
    <div class="input-container">
      <?=form_password( array( 'name' => 'password', 'class' => 'form-password', 'placeholder' => 'Password' ) ); ?>
    </div>
    <?php if ( $attempts ): ?>
      <div class="captcha-input-container">
        <div class="captcha-image"></div>
        <?=form_input( array( 'name' => 'captcha', 'class' => 'form-input', 'placeholder' => 'Enter Captcha Here.', 'autocomplete' => 'off' ) ); ?>
      </div>
      <script type="text/javascript">$(".captcha-image").load("/captcha/")</script>
    <?php endif; ?>
    <div class="remember-container">
      <?=form_checkbox( array( 'name' => 'rememberme', 'class' => 'form-checkbox', 'value' => 1 ) ); ?> Remember Me.
    </div>
    <div class="login-btn-container">
      <span class="login-btn">Login</span>
    </div>
  <?=form_close(); ?>
</div>

<script type="text/javascript">
  $(".login-btn").on("click", function() {
    $(".login-form").submit();
  });
</script>
