<div class="change-password-container">
  <?php if ( validation_errors() ): ?>
    <div class="change-password-errors">
      <?=validation_errors(); ?>
    </div>
  <?php endif; ?>
  <?php if ( $success ): ?>
    <div class="change-password-success">
      Password has been changed!
    </div>
  <?php endif; ?>
  <div class="change-password-contents">
    <h1>Password Change</h1>
    <div class="change-password-form">
      <?=form_open( '/admincp/change_password', array( 'class' => 'change-password-form' ) ); ?>
      <p><?=form_password( array( 'name' => 'current_password', 'class' => 'input-password', 'placeholder' => 'Current Password' ) ); ?></p>
      <p><?=form_password( array( 'name' => 'new_password', 'class' => 'input-password', 'placeholder' => 'New Password' ) ); ?></p>
      <p><?=form_password( array( 'name' => 'confirm_password', 'class' => 'input-password', 'placeholder' => 'Confirm New Password' ) ); ?></p>
      <div class="change-password-btn-container">
        <span class="change-password-btn">Change Password</span>
      </div>
      <?=form_close(); ?>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(".change-password-btn").on("click", function() {
    $(".change-password-form").submit();
  });
</script>
