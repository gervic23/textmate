<?php if ( $keywords ) { krsort( $keywords->logs ); } ?>

<div class="logs-container">
  <div class="search-logs">
    <h1>Search Keywords Logs</h1>
    <div class="search-logs-contents">
      <?php if ( $keywords ): ?>
        <ul>
          <?php foreach ( $keywords->logs as $keyword ): ?>
            <li class="list">
              <ul>
                <li>Keywords: <?=$keyword->keywords; ?></li>
                <li>IP Address: <?=$keyword->ip_address; ?></li>
                <li>Date: <?=$keyword->date; ?></li>
              </ul>
            </li>
          <?php endforeach; ?>
        </ul>
      <?php else: ?>
        <div class="logs-empty">No Search Keywords Logs at this time.</div>
      <?php endif; ?>
    </div>
    <div class="search-btn-container">
      <span class="search-btn">Clear Log</span>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(".search-btn").on("click", function() {
    var c = confirm( "Are you sure you want to clear search logs?" );

    if ( c ) {
      var data = { "log": "search" };

      $.post("/ajax/clear_logs/", data, function() {
        $(".search-logs-contents").html( '<div class="logs-empty">No Search Keywords Logs at this time.</div>' );
      });
    }
  });
</script>
