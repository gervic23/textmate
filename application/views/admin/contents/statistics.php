<div class="stats-container">
  <div class="stats-counts">
    <ul>
      <li>
        <div class="stats-page-load-count"><?=$page_load_count; ?></div>
        <div class="stats-page-title">Total Page Load.</div>
      </li>
      <li>
        <div class="stats-unique-count"><?=$unique_count; ?></div>
        <div class="stats-page-title">Total Unique Visitors</div>
      </li>
    </ul>
    <div class="clear"></div>
  </div>
  <div class="stats-details">
    <h1>Statistics Details</h1>
    <div class="stats-details-contents"></div>
  </div>
  <div class="stats-btn-container">
    <span class="stats-clear-btn">Clear Statistics</span>
  </div>
</div>

<script type="text/javascript">
  $(".stats-details-contents").html( '<img class="stats-loader" src="/pub/img/loading3.gif" />' );
  $(".stats-details-contents").load( '/ajax/load_statistics/' );

  stats_details_size();

  $(window).resize(function() {
    stats_details_size();
  });

  $(".stats-clear-btn").on("click", function() {
    var c = confirm( "Are you sure you want to clear statistics?" );

    if ( c ) {
      var data = { "log": "statistics" };

      $.post("/ajax/clear_logs/", data, function() {
        $(".stats-page-load-count").html( 0 );
        $(".stats-unique-count").html( 0 );
        $(".stats-details-contents").html( '<div class="nostats">There are no statistics at this time.</div>' );
      });
    }
  });

  function stats_details_size() {
    var win_height = $(window).height();
    var new_height = win_height - 350;

    $(".stats-details-contents").css({
      "height": new_height + "px"
    });
  }
</script>
