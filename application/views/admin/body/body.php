<?=doctype(); ?>
<html lang="en-US" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title><?php echo $title; ?></title>
  <link href="/pub/img/mytextmate-icon.png" rel="shortcut icon" />
  <link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Aldrich' rel='stylesheet'>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
  <link rel="canonical" href="<?php echo $canonical; ?>" />
  <link rel="stylesheet" href="/pub/css/admin.css" />
  <script type="text/javascript" src="/pub/js/jquery-3.1.1.min.js"></script>
  <script type="text/javascript">console.log("%cThere are nothing to see here.", "color: blue; font-size: 20px;");</script>
</head>
  <body>
    <header class="main-header">
      &nbsp;
    </header>
    <header class="mobile-header">
      <span class="menu-btn menu"></span>
    </header>
    <?php if ( $this->session->userdata( 'logged_in' ) ): ?>
      <nav class="mobile-nav">
        <ul>
          <li><a href="/admincp/dashboard/">Dashboard</a></li>
          <li><a href="/admincp/posts/">Textmate Posts</a></li>
          <li><a href="/admincp/logs/">Logs</a></li>
          <li><a href="/admincp/statistics/">Statistics</a></li>
          <li><a href="/admincp/account_panel/">Account Panel</a></li>
          <li><a href="/admincp/change_password/">Change Password</a></li>
          <li><a href="/admincp/logout/">Logout</a></li>
        </ul>
      </nav>
    <?php endif; ?>
    <div class="main-container">
      <?php if ( $this->session->userdata( 'logged_in' ) ): ?>
        <div class="left-nav">
          <ul>
            <li><a href="/admincp/dashboard/">Dashboard</a></li>
            <li><a href="/admincp/posts/">Textmate Posts</a></li>
            <li><a href="/admincp/logs/">Logs</a></li>
            <li><a href="/admincp/statistics/">Statistics</a></li>
            <li><a href="/admincp/account_panel/">Account Panel</a></li>
            <li><a href="/admincp/change_password/">Change Password</a></li>
            <li><a href="/admincp/logout/">Logout</a></li>
          </ul>
        </div>
      <?php endif; ?>
      <div class="main-contents">
        <?php $this->load->view('admin/contents/'.$page); ?>
      </div>
      <div class="clear"></div>
    </div>

    <script type="text/javascript">
      var m_bool = false;

      if ( $(window).width() > 900 ) {
        container_resize();

        $(window).resize(function() {
          container_resize();
        });
      }

      $(".menu-btn").on("click", function() {
        if ( m_bool == false ) {
          m_bool = true;

          $(".mobile-nav").fadeIn();
        } else {
          m_bool = false;

          $(".mobile-nav").fadeOut();
        }
      });

      function container_resize() {
        var header_height = $(".main-header").height() + 10;
        var win_height = $(window).height();
        var container_height = win_height - header_height - 59;

        $(".main-container").css({
          "margin-top": header_height + "px",
          "height": container_height + "px"
        });
      }

      <?php if ( $this->session->userdata( 'logged_in' ) ): ?>
        arrange_containers();

        $(window).resize(function() {
          arrange_containers();
        });

        function arrange_containers() {
          var header_height = $(".main-header").height() + 10;
          var win_height = $(window).height();
          var container_height = win_height - header_height - 59;

          $(".left-nav").css({
            "height": container_height + "px"
          });

          $(".main-contents").css({
            "height": container_height + "px"
          });
        }
      <?php endif; ?>
    </script>

    <footer class="main-footer">
      Textmate Created by <a target="_blank" href="https://www.facebook.com/gervic23">Gervic</a>. copyright &copy; <?php echo date("Y"); ?>
    </footer>
  </body>
</html>
