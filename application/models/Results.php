<?php

class Results extends CI_Model {
  public function basic( $str ) {
    $q = $this->db->query( $str );
    return $q->num_rows();
  }

  public function post_num( $cat ) {
    $this->db->from( 'textmate' );
    $this->db->where( 'network', $cat );
    $q = $this->db->get();
    return $q->num_rows();
  }

  public function user_name( $cat ) {
    $this->db->from( 'textmate' );
    $this->db->where( 'network', $cat );
    $this->db->order_by( 'id', 'DESC' );
    $q = $this->db->get();
    $q = $q->result();

    return $q[0]->name;
  }

  public function last_post( $cat ) {
    $this->db->from( 'textmate' );
    $this->db->where( 'network', $cat );
    $this->db->order_by( 'id', 'DESC' );
    $q = $this->db->get();
    $q = $q->result();

    return $q[0]->message;
  }

  public function posts( $cat ) {
    $this->db->from( 'textmate' );
    $this->db->where( 'network', $cat );
    $this->db->order_by( 'id', 'DESC' );
    $this->db->limit( 50, 0 );
    $q = $this->db->get();

    return $q->result();
  }

  public function more_posts( $network, $rows ) {
    $this->db->from( 'textmate' );
    $this->db->where( 'network', $network );
    $this->db->order_by( 'id', 'DESC' );
    $this->db->limit( $rows, 0 );
    $q = $this->db->get();

    return $q->result();
  }

  public function submit_post( $data ) {
    $this->db->insert( 'textmate', $data );
  }

  public function search_count( $keywords ) {
    $this->db->from( 'textmate' );

    $i = 0;

    foreach ( $keywords as $keyword ) {
      $i++;

      if ( $i == 1 ) {
        $this->db->like( 'name', $keyword );
        $this->db->or_like( 'gender', $keyword );
        $this->db->or_like( 'network', $keyword );
        $this->db->or_like( 'message', $keyword );
      } else {
        $this->db->or_like( 'name', $keyword );
        $this->db->or_like( 'gender', $keyword );
        $this->db->or_like( 'network', $keyword );
        $this->db->or_like( 'message', $keyword );
      }
    }

    $q = $this->db->get();
    return $q->num_rows();
  }

  public function search_posts( $keywords ) {
    $this->db->from( 'textmate' );

    $i = 0;

    foreach ( $keywords as $keyword ) {
      $i++;

      if ( $i == 1 ) {
        $this->db->like( 'name', $keyword );
        $this->db->or_like( 'gender', $keyword );
        $this->db->or_like( 'network', $keyword );
        $this->db->or_like( 'message', $keyword );
      } else {
        $this->db->or_like( 'name', $keyword );
        $this->db->or_like( 'gender', $keyword );
        $this->db->or_like( 'network', $keyword );
        $this->db->or_like( 'message', $keyword );
      }
    }

    $this->db->order_by( 'id', 'DESC' );
    $this->db->limit( 50, 0 );

    $q = $this->db->get();
    return $q->result();
  }

  public function search_more_post( $keywords, $rows ) {
    $this->db->from( 'textmate' );

    $i = 0;

    foreach ( $keywords as $keyword ) {
      $i++;

      if ( $i == 1 ) {
        $this->db->like( 'name', $keyword );
        $this->db->or_like( 'gender', $keyword );
        $this->db->or_like( 'network', $keyword );
        $this->db->or_like( 'message', $keyword );
      } else {
        $this->db->or_like( 'name', $keyword );
        $this->db->or_like( 'gender', $keyword );
        $this->db->or_like( 'network', $keyword );
        $this->db->or_like( 'message', $keyword );
      }
    }

    $this->db->order_by( 'id', 'DESC' );
    $this->db->limit( $rows, 0 );

    $q = $this->db->get();
    return $q->result();
  }

  public function count_login_attempts( $ip ) {
    $this->db->from( 'attempts' );
    $this->db->where( 'ip', $ip );
    $q = $this->db->get();
    return $q->num_rows();
  }

  public function insert_captcha( $data ) {
    $this->db->insert( 'captcha', $data );
  }

  public function insert_attempts( $attempt ) {
    $this->db->insert( 'attempts', $attempt );
  }

  public function username_check( $username ) {
    $this->db->from( 'admin' );
    $this->db->where( 'username', $username );
    $q = $this->db->get();
    return $q->num_rows();
  }

  public function chech_login_details( $username, $password ) {
    $this->db->select( 'password' );
    $this->db->from( 'admin' );
    $this->db->where( 'username', $username );
    $this->db->where( 'password', $password );
    $q = $this->db->get();
    return $q->num_rows();
  }

  public function check_captcha( $captcha ) {
    $this->db->from( 'captcha' );
    $this->db->where( 'word', $captcha );
    $q = $this->db->get();
    return $q->num_rows();
  }

  public function remove_login_attempts( $ip ) {
    $this->db->where( 'ip', $ip );
    $this->db->delete( 'attempts' );
  }

  public function exp_captcha( $exp ) {
    $where = array( 'captcha_time <' => $exp );

    $this->db->from( 'captcha' );
    $this->db->where( $where );
    $q = $this->db->get();
    return $q->result();
  }

  public function delete_captcha( $time ) {
    $this->db->where( 'captcha_time', $time );
    $this->db->delete( 'captcha' );
  }

  public function remove_exp_login_attempts( $exp ) {
    $where = array( 'exp <' => $exp );
    $this->db->where( $where );
    $this->db->delete( 'attempts' );
  }

  public function get_new_post() {
    $this->db->select( 'name, network, message, date' );
    $this->db->from( 'textmate' );
    $this->db->order_by( 'id', 'DESC' );
    $q = $this->db->get();
    return $q->result()[0];
  }

  public function cat_counts( $cat ) {
    $this->db->from( 'textmate' );
    $this->db->where( 'network', $cat );
    $q = $this->db->get();
    return $q->num_rows();
  }

  public function insert_stats( $data ) {
    $this->db->insert( 'statistics', $data );
  }

  public function get_stats_count() {
    $this->db->from( 'statistics' );
    $q = $this->db->get();
    return $q->num_rows();
  }

  public function get_messages() {
    $this->db->from( 'textmate' );
    $this->db->order_by( 'id', 'DESC' );
    $this->db->limit( 50, 0 );
    $q = $this->db->get();
    return $q->result();
  }

  public function load_more_messages( $rows ) {
    $this->db->from( 'textmate' );
    $this->db->order_by( 'id', 'DESC' );
    $this->db->limit( $rows, 0 );
    $q = $this->db->get();
    return $q->result();
  }

  public function del_msg( $id ) {
    $this->db->where( 'id', $id );
    $this->db->delete( 'textmate' );
  }

  public function get_page_load_count() {
    $this->db->from( 'statistics' );
    $q = $this->db->get();
    return $q->num_rows();
  }

  public function get_statistics_details() {
    $this->db->from( 'statistics' );
    $this->db->order_by( 'id', 'DESC' );
    $q = $this->db->get();
    return $q->result();
  }

  public function clear_statistics() {
    $this->db->query( 'DELETE FROM `statistics`' );
  }

  public function get_admin_list() {
    $this->db->from( 'admin' );
    $this->db->order_by( 'id', 'DESC' );
    $q = $this->db->get();
    return $q->result();
  }

  public function get_admin_password( $username ) {
    $this->db->select( 'password' );
    $this->db->from( 'admin' );
    $this->db->where( 'username', $username );
    $q = $this->db->get();
    $q = $q->result();
    return $q[0]->password;
  }

  public function update_password( $data, $username ) {
    $this->db->where( 'username', $username );
    $this->db->update( 'admin', $data );

    return true;
  }
}
